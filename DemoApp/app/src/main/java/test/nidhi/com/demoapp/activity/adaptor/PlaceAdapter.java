package test.nidhi.com.demoapp.activity.adaptor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import test.nidhi.com.demoapp.activity.Model.PlacesResultData;
import test.nidhi.com.demoapp.activity.R;


/**
 * Created by nidhishukla on 17/11/16.
 */
public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {

    private Context context;
    private ArrayList<PlacesResultData> arrayList;
    private LayoutInflater inflater;

    public PlaceAdapter(Context context, ArrayList<PlacesResultData> arrayList) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.arrayList = arrayList;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ArrayList<PlacesResultData> getArrayList() {
        return arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_place_layout, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final PlacesResultData placeValue = arrayList.get(position);
        viewHolder.placeTextView.setText(placeValue.getName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView placeTextView;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            placeTextView = (TextView) itemLayoutView.findViewById(R.id.placeTextView);
        }
    }

}