package test.nidhi.com.demoapp.activity.DatabaseManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by nidhishukla on 17/11/16.
 */

public class DbHelper extends SQLiteOpenHelper {


    private static String DATABASE_NAME = "PLACE_DB";
    private SQLiteDatabase database;
    private static int DATABASE_VERSION = 1;
    /**
     * Tables
     */
    private final String TABLE_PLACE = "Place";

    /**
     * Coloumns
     */
    private final String place_id = "place_id";
    private final String place_name = "place_name";
    private final String data = "data";


    String CREATE_PLACE_TABLE = "CREATE TABLE " + TABLE_PLACE + "(" + place_id
            + " INTEGER PRIMARY KEY AUTOINCREMENT , " + place_name + " TEXT , " + data + " TEXT  )";


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        database = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PLACE_TABLE);
    }

    public void insertPlace(String placeName,String stringData) {
        ContentValues values = new ContentValues();
        values.put(place_name, placeName);
        values.put(data, stringData);
        database.insert(TABLE_PLACE, null, values);
    }

    public ArrayList<String> getPlaceToSearch(String keyToSearch) {
        ArrayList<String> result = new ArrayList<>();

        Cursor cursor = database.query(true, TABLE_PLACE, new String[]{
                        place_name}, place_name + " LIKE ?",
                new String[]{keyToSearch + "%"}, null, null, null,
                null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                result.add(cursor.getString(cursor
                        .getColumnIndex(place_name)));
                cursor.moveToNext();
            }

        }

        return result;
    }


    public String getSearchData(String keyToSearch) {
        String result = "";

        Cursor cursor = database.query(true, TABLE_PLACE, new String[]{
                        place_name,data}, place_name + " LIKE ?",
                new String[]{keyToSearch + "%"}, null, null, null,
                null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {

                result =cursor.getString(cursor
                        .getColumnIndex(data));
                cursor.moveToNext();
            }

        }

        if(cursor != null){
            cursor.close();
        }
        return result;
    }




    public ArrayList<String> getAllPlaceToSearch() {
        ArrayList<String> result = new ArrayList<>();

        Cursor cursor = database.rawQuery("select * from "+TABLE_PLACE,null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                result.add(cursor.getString(cursor
                        .getColumnIndex(place_name)));
                cursor.moveToNext();
            }

        }
        if(cursor != null){
            cursor.close();
        }

        return result;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
