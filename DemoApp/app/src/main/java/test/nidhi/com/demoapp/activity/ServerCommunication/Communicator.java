package test.nidhi.com.demoapp.activity.ServerCommunication;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;

import test.nidhi.com.demoapp.activity.Model.Places;

/**
 * Created by nidhishukla on 17/11/16.
 */

public class Communicator {


    public  void call(final Context mContext,String url, HashMap<String,String>params,final TaskCompleter taskCompleter){

        GsonPostRequest gsonObjRequest = new GsonPostRequest<Places>(url,Places.class, null, params, new Response.Listener<Places>() {

            @Override
            public void onResponse(Places response) {



                taskCompleter.onTaskCompleter(response);
//                LogUtils.i("TAG", response.toString());
//                if(requestType != RequestType.REQ_PENDINGREQUEST_HOME){
//                    hideProgressDialog();
//                }
//



            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                String errorString="";
//                if (error instanceof NetworkError){
//                    errorString="Network Error";
//                }
//
////                else if (error instanceof ClientError) {
////
////                }
//                else if (error instanceof ServerError) {
//                    errorString="Server Error";
//                } else if (error instanceof AuthFailureError) {
//                    errorString="Auth Failure Error";
//                } else if (error instanceof ParseError) {
//                    errorString="Parsing Error";
//                } else if (error instanceof NoConnectionError) {
//                    errorString="NoConnection Error";
//                } else if (error instanceof TimeoutError) {
//                    errorString="Timeout Error";
//                } else if (error instanceof VolleyError) {
//                    errorString="Somet";
//                }
//                LogUtils.e("Error", errorString);
                Toast.makeText(mContext,"Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
//                if(requestType != RequestType.REQ_PENDINGREQUEST_HOME){
//                    hideProgressDialog();
//                }
            }
        });

        gsonObjRequest.setTag("TAG");
        Volley.newRequestQueue(mContext).add(gsonObjRequest);

    }
}
