package test.nidhi.com.demoapp.activity.Model;

import java.io.Serializable;

/**
 * Created by nidhishukla on 17/11/16.
 */

public class Geometry implements Serializable {
    Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
