package test.nidhi.com.demoapp.activity.Model;

import java.io.Serializable;

/**
 * Created by nidhishukla on 17/11/16.
 */

public class Location implements Serializable{

    double lat;
    double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
