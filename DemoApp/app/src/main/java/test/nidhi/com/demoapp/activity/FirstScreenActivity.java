package test.nidhi.com.demoapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import test.nidhi.com.demoapp.activity.DatabaseManager.DbHelper;
import test.nidhi.com.demoapp.activity.Model.Places;
import test.nidhi.com.demoapp.activity.ServerCommunication.Communicator;
import test.nidhi.com.demoapp.activity.ServerCommunication.TaskCompleter;
import test.nidhi.com.demoapp.activity.adaptor.PlaceAdapter;

//https://maps.googleapis.com/maps/api/place/textsearch/xml?query=london&key=AIzaSyAMn5sEx4PDLjohvwI2_oCV6MPGwYBilgo
//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&keyword=cruise&key=AIzaSyAMn5sEx4PDLjohvwI2_oCV6MPGwYBilgo
//4F:18:65:51:B1:49:18:3E:3E:6C:A7:A8:5A:79:CC:ED:03:3A:16:01
public class FirstScreenActivity extends AppCompatActivity implements View.OnClickListener, TaskCompleter {

    String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?key=AIzaSyAMn5sEx4PDLjohvwI2_oCV6MPGwYBilgo";
    Button mSearchButton;
    private ArrayList<String> placeArrayList = new ArrayList<>();
    //private RecyclerView mRecyclerView;
    private PlaceAdapter placeAdapter;
    private AutoCompleteTextView searchEditText;
    private ProgressDialog mProgresssDialog;
    ArrayAdapter<String> mAutoAdapter;
    DbHelper mDbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen1);
        //mRecyclerView = (RecyclerView) findViewById(R.id.searchRecyclerView);
        searchEditText = (AutoCompleteTextView) findViewById(R.id.searchEditText);
        mSearchButton = (Button) findViewById(R.id.searchButton);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, placeArrayList);
        searchEditText.setAdapter(adapter);
         mDbHelper = new DbHelper(this);
        getSearchableDataFromDb();
        mSearchButton.setOnClickListener(this);
    }

    private boolean validate(String keyword) {
        if (!TextUtils.isEmpty(keyword)) {
            return true;
        } else {
            Toast.makeText(this, "Please enter key to search.", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    private void showDialog(){
        if(mProgresssDialog == null){
            mProgresssDialog = new ProgressDialog(this);
            mProgresssDialog.setMessage("Please wait...");
        }
        mProgresssDialog.show();

    }

    private void hideDialog(){
        if(mProgresssDialog != null && mProgresssDialog.isShowing()){
            mProgresssDialog.dismiss();
        }
    }

    private void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null && view instanceof EditText) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private void insertData(String searchKey,String data){

        mDbHelper.insertPlace(searchKey,data);
        placeArrayList.add(searchKey);
        if(mAutoAdapter != null){
            mAutoAdapter.notifyDataSetChanged();
        }else{
            setAutoAdapter();
        }

    }


    private void setAutoAdapter(){
        mAutoAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, placeArrayList);
        searchEditText.setAdapter(mAutoAdapter);
        searchEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Gson gson = new Gson();

                String placeData = mDbHelper.getSearchData(placeArrayList.get(position));
                if(!TextUtils.isEmpty(placeData)){
                    Places data = gson.fromJson(placeData, Places.class);
                    Intent in = new Intent(FirstScreenActivity.this, MapsActivity.class);
                    in.putExtra("searchPlace", data);
                    startActivity(in);
                }

            }
        });

    }


    private void getSearchableDataFromDb(){
        placeArrayList=mDbHelper.getAllPlaceToSearch();

        if(placeArrayList!=null && !placeArrayList.isEmpty()) {
            setAutoAdapter();
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.searchButton:
                if (validate(searchEditText.getText().toString())) {
                    hideKeyboard();
                    String searchKey = searchEditText.getText().toString();

                    if (searchKey.contains(" ")) {
                        searchKey = searchKey.replace(" ", "%20");
                    }
                    showDialog();
                    HashMap<String, String> params = new HashMap<>();
                    url = "https://maps.googleapis.com/maps/api/place/textsearch/json?key=AIzaSyAMn5sEx4PDLjohvwI2_oCV6MPGwYBilgo" + "&query=" + searchKey;
                    Communicator communicator = new Communicator();
                    communicator.call(this, url, params, this);
                }
                break;
        }
    }


    @Override
    public void onTaskCompleter(Object object) {
        Places places = (Places) object;
        hideDialog();
        if (places != null && places.getResults() != null && !places.getResults().isEmpty()) {
            placeAdapter = new PlaceAdapter(this, places.getResults());
            //mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            //mRecyclerView.setAdapter(placeAdapter);
            Gson gson = new Gson();
            String responseString = gson.toJson(places);
            String searchKey = searchEditText.getText().toString();
            insertData(searchKey,responseString);
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("searchPlace", places);
            startActivity(in);

        } else {
            Toast.makeText(this, "No record found.", Toast.LENGTH_SHORT).show();
        }

    }
}
