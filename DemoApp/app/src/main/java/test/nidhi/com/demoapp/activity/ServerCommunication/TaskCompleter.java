package test.nidhi.com.demoapp.activity.ServerCommunication;

/**
 * Created by nidhishukla on 17/11/16.
 */

public interface TaskCompleter {

    public void onTaskCompleter(Object object);
}
