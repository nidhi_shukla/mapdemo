package test.nidhi.com.demoapp.activity.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by nidhishukla on 17/11/16.
 */

public class Places implements Serializable {

   ArrayList<PlacesResultData> results=new ArrayList<>();

   public ArrayList<PlacesResultData> getResults() {
      return results;
   }

   public void setResults(ArrayList<PlacesResultData> results) {
      this.results = results;
   }
}
