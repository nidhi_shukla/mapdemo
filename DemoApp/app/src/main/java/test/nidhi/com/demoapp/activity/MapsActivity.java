package test.nidhi.com.demoapp.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import test.nidhi.com.demoapp.activity.Model.Places;
import test.nidhi.com.demoapp.activity.Model.PlacesResultData;
import test.nidhi.com.demoapp.activity.adaptor.HorizontalAdapter;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback ,GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private Places mPlaces;
    RecyclerView horizontal_recycler_view;
    List<String> imagesList = new ArrayList<String>();

    URL url;
    URLConnection urlconnection ;
    int FileSize;
    InputStream inputstream;
    OutputStream outputstream;
    byte dataArray[] = new byte[1024];
    long totalSize = 0;
    ImageView imageview;
    String GetPath ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        horizontal_recycler_view = (RecyclerView)findViewById(R.id.gallery_recycler_view);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(MapsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);

        if(getIntent() != null && getIntent().getExtras() != null){
            mPlaces = (Places)getIntent().getSerializableExtra("searchPlace");
        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if(mPlaces != null){
            ArrayList<PlacesResultData> placeList = mPlaces.getResults();
            for(PlacesResultData place :placeList){
                googleMap.setOnMarkerClickListener(MapsActivity.this);
                LatLng searchedPlace = new LatLng(place.getGeometry().getLocation().getLat(),place.getGeometry().getLocation().getLng());
                mMap.addMarker(new MarkerOptions().position(searchedPlace).title(place.getName().toString()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(searchedPlace));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(searchedPlace, 17.0f));
                imagesList.add(place.getIcon());
                try{
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                }catch (Exception e){

                }
            }

        }


    }





public void startImageDownloading(String url){

    new MyAsyncTask(url).execute();

}



    @Override
    public boolean onMarkerClick(Marker marker) {
        if(imagesList.size()> 0) {
            HorizontalAdapter adapter = new HorizontalAdapter(this, imagesList);
            horizontal_recycler_view.setAdapter(adapter);
            horizontal_recycler_view.setVisibility(View.VISIBLE);
        }

        return false;
    }




    class MyAsyncTask extends AsyncTask<Void, Void, Void>    {

        public ProgressDialog dialog;
        URL url;

        MyAsyncTask(String imageUrl)
        {
            dialog = new ProgressDialog(MapsActivity.this);
            dialog.setMessage("Downloading image...");
            dialog.setCancelable(true);
            dialog.setIndeterminate(true);
            try {
                url = new URL(imageUrl);
            }catch (Exception e){

            }

        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }
        protected Void doInBackground(Void... arg0) {
            try {


                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);

                //and connect!
                urlConnection.connect();

                String fileName = url.toString().substring(url.toString().lastIndexOf("/")+1,url.toString().length()) ;


                File SDCardRoot = Environment.getExternalStorageDirectory();
                File file = new File(SDCardRoot +"/"+getString(R.string.app_name),fileName);

                FileOutputStream fileOutput = new FileOutputStream(file);

                InputStream inputStream = urlConnection.getInputStream();

                int totalSize = urlConnection.getContentLength();
                int downloadedSize = 0;

                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0; //used to store a temporary size of the buffer

                while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                    fileOutput.write(buffer, 0, bufferLength);
                    downloadedSize += bufferLength;


                }
                //close the output stream when done
                fileOutput.close();

                //catch some possible errors...
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


}





