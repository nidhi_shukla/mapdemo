package test.nidhi.com.demoapp.activity.Model;

import java.io.Serializable;

/**
 * Created by nidhishukla on 17/11/16.
 */

public class PlacesResultData implements Serializable {
    String name = "";
    String icon="";
    Geometry geometry;
    
    public String getIcon() {
		return icon;
	}
    
    public void setIcon(String icon) {
		this.icon = icon;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
