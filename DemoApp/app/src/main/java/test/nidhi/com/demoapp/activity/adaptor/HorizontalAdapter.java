package test.nidhi.com.demoapp.activity.adaptor;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import test.nidhi.com.demoapp.activity.MapsActivity;
import test.nidhi.com.demoapp.activity.R;

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

    private List<String> horizontalList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView placeIcon;

        public MyViewHolder(View view) {
            super(view);
            placeIcon = (ImageView) view.findViewById(R.id.icon_place);


        }
    }


    public HorizontalAdapter(Context ctx,List<String> horizontalList) {
        this.horizontalList = horizontalList;
        this.mContext = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.horizontal_item_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Picasso.with(mContext)
                .load(horizontalList.get(position))
                .into(holder.placeIcon);
        holder.placeIcon.setTag(horizontalList.get(position));
        holder.placeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = v.getTag().toString();
                ((MapsActivity)mContext).startImageDownloading(uri);
            }
        });

    }




    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}